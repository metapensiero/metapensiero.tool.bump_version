.. -*- coding: utf-8 -*-

Changes
-------

1.3 (2017-03-22)
~~~~~~~~~~~~~~~~

- Minor adjustments to take off the dust


1.2 (2016-03-23)
~~~~~~~~~~~~~~~~

- New option to automatically select the versioning schema, when one exists

- New simple test suite


1.1 (2015-09-09)
~~~~~~~~~~~~~~~~

- Properly include needed sources in the distribution


1.0 (2015-08-09)
~~~~~~~~~~~~~~~~

- Reimport from metapensiero.extjs.desktop
