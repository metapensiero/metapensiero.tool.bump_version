# -*- coding: utf-8 -*-
# :Project:   metapensiero.tool.bump_version -- Functional tests
# :Created:   mer 23 mar 2016 08:38:07 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

import tempfile
import shutil
import subprocess
import sys


def extract_examples(f):
    statements = None
    expected_output = None
    line = f.readline()
    while line:
        if line.startswith('  $ '):
            if statements is None:
                statements = []
                expected_output = []
            statements.append(line[4:-1])
        elif line.startswith('  '):
            if expected_output is not None:
                expected_output.append(line[2:-1])
        elif line == '\n':
            if statements:
                yield statements, expected_output
            statements = None
            expected_output = None
        line = f.readline()


def run_example(tmpdir, statements):
    for stmt in statements:
        result = subprocess.check_output(stmt, shell=True, cwd=tmpdir)
    return result.decode('utf-8').rstrip('\n')


def main():
    from io import open

    ok = 0
    failures = 0

    tmpdir = tempfile.mkdtemp()
    try:
        with open('README.rst', encoding='utf-8') as f:
            for statements, expected_output in extract_examples(f):
                try:
                    output = run_example(tmpdir, statements)
                except subprocess.CalledProcessError as e:
                    print('CODE:\n%s\n' % '\n'.join(statements))
                    print(e.output.decode('utf-8'))
                    break
                else:
                    if expected_output:
                        expected_output = '\n'.join(expected_output)
                        if output != expected_output:
                            print('CODE:\n%s\n' % '\n'.join(statements))
                            print('EXPECTED:\n%s\n' % expected_output)
                            print('GOT:\n%s\n' % output)
                            failures += 1
                        else:
                            ok += 1
                    else:
                        ok += 1
    finally:
        shutil.rmtree(tmpdir)

    print('Ok: %d' % ok)
    print('Failures: %d' % failures)

    return failures


if __name__ == '__main__':
    sys.exit(main())
