# -*- coding: utf-8 -*-
# :Project:   metapensiero.tool.bump_version -- Main makefile
# :Created:   dom  9 ago 2015, 14.28.40, CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: Copyright (C) 2015, 2016 Lele Gaifax
#

all: help

help::
	@printf "Development\n"
	@printf "===========\n\n"
	@printf "test\n\tRun test suite\n"

test:
	tox

include Makefile.release
